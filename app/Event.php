<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model implements \MaddHatter\LaravelFullcalendar\Event
{
    protected  $table = 'events';

    protected $fillable = [
     'user_id', 'service_id', 'employee_id', 'title', 'start', 'end'
    ];

    protected $dates = ['start', 'end'];

    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return false;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    public function getEventOptions()
    {
        $temp = $this;
        $temp->description = $this->getDescription();
        return [
            'url' => json_encode($temp, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES)
            //etc
        ];
    }

    public function getDescription()
    {
        $user = User::find($this->user_id);
        $employee = User::find($this->employee_id);
        $str = $this->title;
        $str .= " από ".$employee->getFullName();
        $str .= " στις ".$this->start;
        $str .= " για τον πελάτη ".$user->getFullName();
        return $str;
    }


    public function scopeOfUser($query, $employee_id)
    {
        return $query->where('employee_id', '=', $employee_id);
    }
}

