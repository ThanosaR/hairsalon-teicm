<?php
/**
 * Created by PhpStorm.
 * User: traveler1
 * Date: 17/1/2018
 * Time: 11:41 πμ
 */

namespace App\Http\Controllers;
use App\User;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.users');
    }

    public function getUsers()
    {
        return \DataTables::of(User::query())->make(true);
    }

    public function deleteUser()
    {
        $data = array();
        $data["success"] = false;
        $data["errors"] = array();
        $user_id = Input::get("user_id");
        if(empty($user_id)){
            $data["message"] = "User id is required";
            $data["errors"]["user_id"] = "User id is required";
            return response()->json($data);
        }
        $user = User::find($user_id);

        if(empty($user)){
            $data["message"] = "User not found";
            $data["errors"]["user"] = "User not found";
            return response()->json($data);
        }

        $data["success"] = $user->delete();
        $data["message"] = "Ο χρήστης διαγράφηκε";
        return response()->json($data);
    }

    public function getUser(Request $request, $user_id)
    {
        $user = User::find($user_id);
        if(empty($user)){
            return redirect()->back()->with("error", "User not found");
        }
        return $user;
    }

    public function createUser(Request $request)
    {
        $validatedData = [
            'name'     => 'required|max:255',
            'last_name'=> 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'phone'    => 'required|max:255',
        ];
        $validator = Validator::make($request->all(), $validatedData);


        $data = array();
        $data["success"] = false;
        $user = $request->input();
        // check if user exists and save it in DB

        if ($validator->fails()) {
            $data["errors"] = $validator->errors();
            $data["message"] = "Λάθος εγγραφές.";
            return response()->json($data);
        }
        if(empty($user["id"])){
            {
                $newUser = new User();
                $newUser->name = $user["name"];
                $newUser->last_name = $user["last_name"];
                $newUser->email = $user["email"];
                $newUser->password = bcrypt(uniqid());
                $newUser->address = $user["address"];
                $newUser->discount = $user["discount"];
                $newUser->age = $user["age"];
                $newUser->phone = $user["phone"];
                $newUser->save();
                $data["user"] = $newUser;
                $data["message"] = "Ο χρήστης δημιουργήθηκε επιτυχώς";
            }
        }
        $data["success"] = true;
        return response()->json($data);
    }

    public function updateUser(Request $request)
    {
        $data = array();
        $data["success"] = false;
        $user = $request->input();

        // check if user exists and save it in DB

        if(!empty($user["id"])){
            $original_user = User::find($user["id"]);
            //update
            $original_user->name = $user["name"];
            $original_user->last_name = $user["last_name"];
            $original_user->address = $user["address"];
            $original_user->email = $user["email"];
            $original_user->phone = $user["phone"];
            $original_user->age = $user["age"];
            $original_user->discount = $user["discount"];
            $original_user->save();
            $data["user"] = $original_user;
            $data["message"] = "Ο χρήστης ενημερώθηκε";
        }
        $data["success"] = true;
        return response()->json($data);
    }

}