<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Service;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class RootsController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('/admin/roots');
    }
}