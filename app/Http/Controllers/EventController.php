<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use http\Env\Response;
use Illuminate\Support\Facades\Input;
use App\Service;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;
use Carbon\Carbon;

class EventController extends Controller
{
    public function mine()
    {
        $myid = Auth::id();
        return $this->eventsList($myid);
    }

    public function eventsList($employee_id = null)
    {
        $employee = array();
        if(empty($employee_id)){
            $events = Event::all();
        }else{
            $employee = User::find($employee_id);
            if(!$employee){
                return response()->isNotFound();
            }
            $events = Event::ofUser($employee_id)->get();
        }

        return response()->json($events);
    }

    public function getEvents()
    {
        $events = Event::all();

        return response()->json($events);
    }

    public function index()
    {
        $data = array();
        $data["services"] = Service::all();
        $data["employees"] = User::ofRole(2)->get();
        $data["users"] = User::ofRole(3)->get();

        return view('/admin/fullcalendar2', $data, compact('calendar'));

    }

    public function addEvent()
    {

        $dtStart = Input::get('start');
        $dtEnd = Input::get('end');
        $employee_id = Input::get('employee_id');

        $events_count = Event::where('employee_id', $employee_id)
            ->where(function ($query) use ($dtStart, $dtEnd) {
                $query->whereBetween('end', [$dtStart, $dtEnd]);
                $query->orwhereBetween('start', [$dtStart, $dtEnd]);
                $query->orWhere(function ($query2) use ($dtStart, $dtEnd) {
                    $query2->where('start', '<', $dtStart);
                    $query2->where('end', '>', $dtEnd);
                });
            })
            ->count();

        if ($events_count > 0) {
            $data["succes"] = false;
            $data["message"] = "Ο υπάλληλος που επιλέξατε δεν είναι διαθέσιμος αυτή την ώρα!";

            return response()->json($data);
        } else {

            $service_id = Input::get("service_id");
            $service = Service::find($service_id);
            if (!$service) {
                return $this->errorHandler("Service not found", $service_id);
            }
            $event = new Event;
            $event->id = Input::get('id');
            $event->employee_id = Input::get('employee_id');
            $event->user_id = Input::get('user_id');
            $event->service_id = $service->id;
            $event->title = $service->type;
            $start = \Carbon\Carbon::createFromTimestamp(strtotime(Input::get('start')));
            $event->start = $start->toDateTimeString();
            $event->end = $start->addMinutes($service->time)->toDateTimeString();

            $event->save();

            return $this->successHandler($event, "Το ραντεβού αποθηκεύτηκε επιτυχώς!", "event");
        }
    }

    public function CheckForEvent()
    {
        $events = Event::all();

       return response()->json($events);
    }

    public function delEvent()
    {
        $data = array();
        $data["success"] = false;
        $data["errors"] = array();
        $event_id = Input::get('event_id');
        if(empty($event_id)){
            $data["message"] = "Event id is required";
            $data["errors"]["event_id"] = "Event id is required";
            return response()->json($data);
        }
        $event = Event::find($event_id);

        if(empty($event)){
            $data["message"] = "Event not found";
            $data["errors"]["event"] = "Event not found";
            return response()->json($data);
        }

        $data["success"] = $event->delete();
        $data["message"] = "Το ραντεβού διαγράφηκε";
        return response()->json($data);

    }

    public function updateEvent(Request $request)
    {
        $data = array();
        $data["success"] = false;
        $event = $request->input();

        // check if user exists and save it in DB
        $service_id = Input::get("service_id");
        $service = Service::find($service_id);
        if(!$service){
            return $this->errorHandler("Service not found", $service_id);
        }
        $employee_id = Input::get("employee_id");
        if(!$employee_id){
            return $this->errorHandler("Employee not found", $employee_id);
        }

        if(!empty($event["event_id"])){
            $original_event = Event::find($event["event_id"]);
            //update
            $original_event->id =  $event["event_id"];
            $original_event->employee_id = $event["employee_id"];
            $original_event->user_id = $event["user_id"];
            $original_event->title = $service->type;
            $start = \Carbon\Carbon::createFromTimestamp(strtotime(Input::get('start')));
            $original_event->start = $start->toDateTimeString();
            $original_event->end = $start->addMinutes($service->time)->toDateTimeString();
            $original_event->save();
            $data["event"] = $original_event;
            $data["message"] = "Το ραντεβού ενημερώθηκε";
        }
        $data["success"] = true;
        return response()->json($data);
    }

    public function getEmployees()
    {
        $employees = User::ofRole(2)->get();
        return response()->json($employees);
    }

    public function errorHandler($reason = "", $details = array())
    {
        $data = array();
        $data["success"] = false;
        $data["reason"] = empty($reason) ? "The request failed." : $reason;
        if(!empty($details)){
            $data["details"] = $details;
        }
        return response()->json($data);
    }



    public function successHandler($results = array(), $message = "Success!", $key = "results", $details = array())
    {
        $data = array();
        $data["success"] = true;
        $data[$key] = $results;
        $data["message"] = $message;
        if(!empty($details)){
            $data["details"] = $details;
        }
        return response()->json($data);
    }
}