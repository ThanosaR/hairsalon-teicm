<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;


use App\Service;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Event;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Validator;
use Carbon\Carbon;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    public function index()
    {
        $data = array();
        $data["services"] = Service::all();
        return view('homepage', $data);
    }

    public function indexEvent()
    {
        $data = array();
        $data["services"] = Service::all();
        $data["employees"] = User::ofRole(2)->get();
        $data["users"] = User::ofRole(3)->get();
        $data["user_id"] = Auth::id();
        $data["user"] = Auth::user();

        return view('/events',  $data,   compact('calendar'));

    }

    public function mineUser()
    {
        $myid = Auth::id();
        return $this->eventsListUser($myid);
    }

    public function mineEmployee()
    {
        $myid = Auth::id();
        return $this->eventsListEmployee($myid);
    }

    public function eventsListUser($user_id = null)
    {

        $user = array();
        if(empty($user_id)){
            $events = Event::all();
        }else{
            $user = User::find($user_id);
            if(!$user){
                return $this->errorHandler("User not found", $user);
            }
//            $events = Event::ofUser($user_id)->get();
            $events = Event::where('user_id', $user_id)->get();
//            dd($events);
        }
        return response()->json($events);
    }

    public function eventsListEmployee($employee_id = null)
    {
        $employee = array();
        if(empty($employee_id)){
            $events = Event::all();
        }else{
            $employee = User::find($employee_id);
            if(!$employee){
                return $this->errorHandler("User not found", $employee);
            }
            $events = Event::where('employee_id', $employee_id)->get();
//            dd($events);
        }
        return response()->json($events);
    }

    public function checkUser()
    {
        $data = array();
        $data["user_id"] = Auth::id();
        $data["user"] = Auth::user();
        return response()->json($data);
    }

    public function getUsers()
    {
        $users = User::ofRole(3)->get();
        return response()->json($users);
    }

    public function updateUser(Request $request)
    {
        $data = array();
        $data["success"] = false;
        $user = $request->input();
        // check if user exists and save it in DB

        if(!empty($user["id"])){
            $original_user = User::find($user["id"]);
            //update
            $original_user->name = $user["name"] ;
            $original_user->last_name = $user["last_name"];
            $original_user->address = $user["address"];
//            $original_user->password = bcrypt(uniqid());
            $original_user->email = $user["email"];
            $original_user->phone = $user["phone"];
            $original_user->age = $user["age"];
            $original_user->discount = $user["discount"];
            $original_user->save();
            $data["user"] = $original_user;
            $data["message"] = "Τα προσωπικά σας στοιχεία ενημερώθηκαν";
        }
        $data["success"] = true;
        return response()->json($data);
    }

    public function updatePassword(Request $request)
    {
        $data = array();
        $data["success"] = false;
        $password = $request->input();

        // check if user exists and save it in DB

        if(!empty($password["id"])){
            $original_password = User::find($password["id"]);
            //update
            $original_password->password = bcrypt(uniqid());;
            $original_password->save();
            $data["password"] = $original_password;
            $data["message"] = "Ο κωδικός ενημερώθηκε";
        }
        $data["success"] = true;
        return response()->json($data);
    }

    public function getEmployees()
    {
        $employees = User::ofRole(2)->get();
        return response()->json($employees);
    }

    public function addEvent()
    {
        $dtStart = Input::get('start');
        $dtEnd = Input::get('end');
        $employee_id = Input::get('employee_id');

        $events_count = Event::where('employee_id', $employee_id)
            ->where(function($query) use ($dtStart, $dtEnd){
                $query->whereBetween('end', [$dtStart, $dtEnd]);
                $query->orwhereBetween('start', [$dtStart, $dtEnd]);
                $query->orWhere(function($query2) use ($dtStart, $dtEnd) {
                    $query2->where('start', '<', $dtStart);
                    $query2->where('end', '>', $dtEnd);
                });
            })
            ->count();

        if($events_count > 0) {
            $data["succes"] = false;
            $data["message"] = "Ο υπάλληλος που επιλέξατε δεν είναι διαθέσιμος αυτή την ώρα!";

            return response()->json($data);
        } else{




        $service_id = Input::get("service_id");
        $service = Service::find($service_id);
        if(!$service){
            return $this->errorHandler("Service not found", $service_id);
        }
        $event = new Event;
        $event->id = Input::get('id');
        $event->employee_id = Input::get('employee_id');
        $event->user_id = Input::get('user_id');
        $event->service_id = $service->id;
        $event->title = $service->type;
        $start = \Carbon\Carbon::createFromTimestamp(strtotime(Input::get('start')));
        $event->start = $start->toDateTimeString();
        $event->end = $start->addMinutes($service->time)->toDateTimeString();

        $event->save();

        return $this->successHandler($event, "Το ραντεβού αποθηκεύτηκε επιτυχώς!", "event");
    }
    }

    public function getEvents()
    {
        $events = Event::all();

        return response()->json($events);
    }

    public function registerService()
    {
        $user_id = Auth::id();
        if(empty($user_id)){
            // user is not logged in
        }
    }

    public function errorHandler($reason = "", $details = array())
    {
        $data = array();
        $data["success"] = false;
        $data["reason"] = empty($reason) ? "The request failed." : $reason;
        if(!empty($details)){
            $data["details"] = $details;
        }
        return response()->json($data);
    }

    public function successHandler($results = array(), $message = "Success!", $key = "results", $details = array())
    {
        $data = array();
        $data["success"] = true;
        $data[$key] = $results;
        $data["message"] = $message;
        if(!empty($details)){
            $data["details"] = $details;
        }
        return response()->json($data);
    }
}