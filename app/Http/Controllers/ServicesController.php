<?php
/**
 * Created by PhpStorm.
 * service: traveler1
 * Date: 17/1/2018
 * Time: 11:41 πμ
 */

namespace App\Http\Controllers;
use App\Service;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.services');
    }

    public function getServices()
    {
        return \DataTables::of(Service::query())->make(true);
    }

    public function createService(Request $request)
    {
        $validatedData = [
            'type'      => 'required|max:255',
            'time'      => 'required|max:255',
            'cost'     => 'required|max:255',
        ];
        $validator = Validator::make($request->all(), $validatedData);


        $data = array();
        $data["success"] = false;
        $service = $request->input();
        // check if service exists and save it in DB

        if ($validator->fails()) {
            $data["errors"] = $validator->errors();
            $data["message"] = "Λάθος εγγραφές.";
            return response()->json($data);
        }
        if(empty($service["id"])){
            {
                $newService = new Service();
                $newService->type = $service["type"];
                $newService->time = $service["time"];
                $newService->cost = $service["cost"];
                $newService->save();
                $data["service"] = $newService;
                $data["message"] = "Η εγγραφή δημιουργήθηκε επιτυχώς";
            }
        }
        $data["success"] = true;
        return response()->json($data);
    }

    public function updateService(Request $request)
    {
        $data = array();
        $data["success"] = false;
        $service = $request->input();
        // check if user exists and save it in DB

        if(!empty($service["id"])){
            $original_service = Service::find($service["id"]);
            //update
            $original_service->type = $service["type"];
            $original_service->time = $service["time"];
            $original_service->cost = $service["cost"];
            $original_service->save();
            $data["service"] = $original_service;
            $data["message"] = "Η υπηρεσία ενημερώθηκε";
        }
        $data["success"] = true;
        return response()->json($data);
    }

    public function deleteService()
    {
        $data = array();
        $data["success"] = false;
        $service_id = Input::get("service_id");
        if(empty($service_id)){
            $data["reason"] = "service not found";
            return response()->json($data);
        }
        $service = service::find($service_id);
        if(empty($service)){
            $data["reason"] = "service not found";
            return response()->json($data);
        }

        $data["success"] = $service->delete();
        return response()->json($data);
    }

    public function getService(Request $request, $service_id)
    {
        $service = Service::find($service_id);
        if(empty($service)){
            return redirect()->back()->with("error", "service not found");
        }
        return $service;
    }
}