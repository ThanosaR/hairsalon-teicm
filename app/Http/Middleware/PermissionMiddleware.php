<?php

namespace App\Http\Middleware;

use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param null $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = null)
    {
        if(!$request->user()){
            abort(404, "Πρέπει να συνδεθείς.");
        }
        if ($permission !== null && !$request->user()->can($permission)) {
            abort(404);
        }

        return $next($request);
    }
}
