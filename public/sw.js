const VERSION = 1518568506885;
const BYPASS_FOR_NETWORK = true;

let CURRENT_CACHE_VERSION = 'sw-cache-v';
const REG_EXP       = new RegExp('^'+CURRENT_CACHE_VERSION);
CURRENT_CACHE_VERSION += VERSION;
const EXPECTED_CACHES  = [
    CURRENT_CACHE_VERSION
];
const APP_NAME = "HairSalon";


const URLS_TO_CACHE = [
    // css
    "/css/homepage.css",
    "/css/login.css",
    "/css/owl.carousel.css",
    // js
    "/js/partials/login.js",
    "/js/partials/owlcarousel.js",
    "/js/partials/fullcalendar2.js",
    "/js/partials/fullcalendar2-front.js",
    // img
    "/img/fav-icon.png"
];

const NO_CORS_URLS = [

];

const EXCLUDED_FROM_FETCHHANDLER = [

];



self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CURRENT_CACHE_VERSION).then(function(cache) {
            console.log("Installing");
            return cache.addAll(URLS_TO_CACHE)
                .then(function() {
                    return Promise.all(
                        NO_CORS_URLS.map(function(url) {
                            let req = new Request(url);
                            fetch(req.clone(), {
                                mode: "no-cors"
                            }).then(function(res) {
                                return cache.put(req, res)
                            }).catch(function(err) {
                                console.warn(err);
                                return Promise.resolve();
                            });
                        })
                    )
                });
        })
            .then(function() {
                console.log("Taking control.");
                self.skipWaiting();
            }).catch(function(e){
            console.log("Error! ",e);
        })
    );
});

self.addEventListener('activate', function(event) {
    // remove caches beginning "18-24-cache-" that aren't in
    // expectedCaches
    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cacheName) {
                    if (!REG_EXP.test(cacheName)) {
                        return;
                    }
                    if (EXPECTED_CACHES.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            );
        }).then(function() {
            console.log("Activated");
            return self.clients.claim();
        }).catch(function(e){
            console.log("Error! ",e);
        })
    );
});

function errorResponse(request, error, response){
    var status = response ? response.status || 400 : 400;
    var statusText = response ? response.statusText || "The request failed!" : error || "The request failed!";
    var body, options, accepts = request.headers.get('accept');
    if(accepts.includes('application/json')) {
        body = {"error": statusText};
        options = {
            "headers": new Headers({"Content-Type": "application/json"}),
            "status" : status,
            "statusText" : statusText
        };
    }else if(accepts.includes('text/html') || accepts.includes('text/plain')){
        body = statusText;
        options = {
            headers: new Headers({"Content-Type": "text/plain"}),
            "status": status,
            "statusText": statusText
        };
    }else if(accepts.includes('image')){
        return caches.match('/assets/img/image-not-found.jpg').then(function(response) {
            return new Response(response.body,
                {
                    status : status,
                    statusText: statusText,
                    headers: new Headers({"Content-Type": "image/jpg"})
                }
            );
        });
    }else{
        body = statusText;
        options = {
            headers: new Headers({"Content-Type": "text/plain"}),
            "status": status,
            "statusText": statusText
        };
    }
    return new Response(body, options);
}

self.addEventListener('fetch', function(event) {
    if (BYPASS_FOR_NETWORK){
        return;
    }
    if (event.request.method !== 'GET' || event.request.headers.get('accept').includes('application/json')) {
        return;
    }
    var requestURL = new URL(event.request.url);
    if(EXCLUDED_FROM_FETCHHANDLER.indexOf(requestURL.pathname) > -1) {
        return;
    }
    if(["/css", "/js", "/img"].indexOf(requestURL.pathname) === -1) {
        return;
    }
    if (requestURL.origin === location.origin) {
        event.respondWith(
            caches.open(CURRENT_CACHE_VERSION).then(function (cache) {
                return cache.match(event.request).then(function(response) {
                    if(response){
                        return response;
                    }
                    var fetchPromise = fetch(event.request.clone()).then(function(networkResponse) {
                        if (!networkResponse.ok) {
                            if(networkResponse.type === "opaqueredirect"){
                                // Let redirects pass through
                                return networkResponse;
                            }
                            return errorResponse(event.request, null, networkResponse);
                        }
                        // Cache the request on conditions
                        if(/^(\/css|\/img\/js)/.test(requestURL.pathname) && !/\?[a-zA-Z]=[a-zA-Z0-9.]+/.test(requestURL.href)){
                            console.log("Extra CACHE: "+requestURL.href);
                            cache.put(event.request, networkResponse.clone());
                        }
                        return networkResponse;
                    });
                    return fetchPromise || response;
                })
                    .catch(function (err) {
                        return errorResponse(event.request,err);
                    });
            })
        );
        return;
    }
    if (requestURL.hostname === 'fonts.googleapis.com') {
        event.respondWith(
            caches.open(CURRENT_CACHE_VERSION).then(function(cache) {
                return cache.match(event.request).then(function (cacheResponse) {
                    return cacheResponse ||
                        fetch(event.request.clone()).then(function (response) {
                            if(response){
                                console.log("Extra FONT: "+requestURL.href);
                                cache.put(event.request.clone(), response.clone());
                            }
                            return response;
                        });
                })
                    .catch(function (err) {
                        return errorResponse(event.request, err);
                    });
            })
        );
        return;
    }
    if(requestURL.protocol === "http:"){
        return;
    }
    event.respondWith(
        caches.open(CURRENT_CACHE_VERSION).then(function(cache) {
            return cache.match(event.request).then(function(cacheResponse) {
                return cacheResponse || fetch(event.request);
            }).catch(function(err){
                return errorResponse(event.request, err);
            });
        })
    );
});
