$(document).ready(function() {

    Date.prototype.addMinutes = function(minutes){
        this.setTime(this.getTime() + minutes*60000);
    };

    Date.prototype.addDays = function(days){
        this.setDate(this.getDate() + days);
    };

    Date.prototype.toDateTimeString = function(){
        var month = this.getMonth() + 1;
        if(month <= 9){
            month = "0" + month;
        }
        var date = this.getDate();
        if(date <= 9){
            date = "0" + date;
        }
        var hours = this.getHours();
        if(hours <= 9){
            hours = "0" + hours;
        }
        var minutes = this.getMinutes();
        if(minutes <= 9){
            minutes = "0" + minutes;
        }
        var strTime = hours + ':' + minutes;
        return this.getFullYear()+ "-" + month + "-" + date  + " " + strTime;
    };

    var today = new Date();
    var tomorrow = new Date();
    tomorrow.addDays(1);
    tomorrow.setHours(9);
    tomorrow.setMinutes(0);

    // Date Range Picker Configuration
    var notWorkingDays = [0];
    $('input[name="daterange"]').daterangepicker({
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: false,
        timePickerIncrement: 10,
        singleDatePicker: true,
        autoApply: true,
        minTime:'11:00',
        minDate: today,
        startDate: tomorrow,
        locale: {
            format: 'YYYY-MM-DD HH:mm',
            cancelLabel:'Άκύρωση',
            applyLabel: 'Υποβολή'
        },
        isInvalidDate: function(date) {
            return (notWorkingDays.indexOf(date.day()) !== -1 || (date.hour() < 9 || date.hour() > 20));
        }
    });

    var sources = {
          cal_sources: {
                url: "/events/getEvents",
                type: 'GET',
                dataType:"json",
                encode: true,
              success: function(data){
                  var allEvents = data;
                  // console.log(allEvents);
              },
                error: function(data) {
                    console.log(data);
                    alertify.error(data.responseText);
                }

            },

        mineUser:   {
            url: "/events/mineUser",
            type: 'GET',
            dataType:"json",
            encode: true,
            error: function(data) {
                alertify.error(data.responseText);
            }
        },

        mineEmployee: {
            url: "/events/mineEmployee",
            type: 'GET',
            dataType: "json",
            encode: true,
            error: function (data) {
                alertify.error(data.responseText);
            }
        }
    };

    
    $('.userList').click(function () {
        $( ".fc-myCustomButton-button" ).css( "background-color", "red" );
        $(".fc-myCustomButton-button").prop('disabled',true);

        //Hide Calendar
        $("#calendar").hide();
        //Refetch Events
        $("#calendar").fullCalendar('removeEventSource', sources.cal_sources);
        $("#calendar").fullCalendar('addEventSource', sources.mineUser);

        setTimeout(function(){
            //Show the Calendar
            $("#calendar").show();
            //Render
            $("#calendar").fullCalendar('render');

        },2000);
    });

    $('.employeeList').click(function () {
        $( ".fc-myCustomButton-button" ).css( "background-color", "red" );
        $(".fc-myCustomButton-button").prop('disabled',true);

        //Hide Calendar
        $("#calendar").hide();
        //Refetch Events
        $("#calendar").fullCalendar('removeEventSource', sources.cal_sources);
        $("#calendar").fullCalendar('addEventSource', sources.mineEmployee);

        setTimeout(function(){
            //Show the Calendar
            $("#calendar").show();
            //Render
            $("#calendar").fullCalendar('render');

        },2000);
    });
        

        


    $('#calendar').fullCalendar({
        // customButtons: {
        //     myCustomButton: {
        //         text: 'Τα ραντεβού μου',
        //         click: function() {
        //             $( ".fc-myCustomButton-button" ).css( "background-color", "red" );
        //             $(".fc-myCustomButton-button").prop('disabled',true);
        //
        //             //Hide Calendar
        //             $("#calendar").hide();
        //             //Refetch Events
        //             $("#calendar").fullCalendar('addEventSource', sources.cal_sources);
        //             $("#calendar").fullCalendar('removeEventSource', sources.mine);
        //
        //             setTimeout(function(){
        //                 //Show the Calendar
        //                 $("#calendar").show();
        //                 //Render
        //                 $("#calendar").fullCalendar('render');
        //
        //             },2000);
        //         }
        //     }
        // },
        header: {
            left: 'prev,next myCustomButton',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        buttonText: {
            today: 'today',
            month: 'Μήνας',
            week: 'Εβδομάδα',
            day: 'Ημέρα'
        },
        agendaEventMinHeight: 40,
        eventLimit: true, // for all non-agenda views
        firstDay:1,
        height: "auto",
        minTime: "09:00:00",
        maxTime: "18:00:00",
        eventSources: [ sources.cal_sources],
        textColor: 'white', // a non-ajax option
        editable: false,
        businessHours: [{
            // days of week. an array of zero-based day of week integers (0=Sunday)
            dow: [ 1, 2, 4, 5], // Monday - Thursday

            start: '9:00', // a start time (10am in this example)
            end: '18:00' // an end time (6pm in this example)
        },
        {
            dow: [6], // Saturday

            start: '9:00',
            end: '14:00'
        }],
        slotEventOverlap: false,
        eventConstraint: "businessHours",
        startEditable:true,
        durationEditable:true,
        allDay:false,

        eventClick: function(event) {
            this.event = event;
            var event_id = this.event.id;

            $('#modal-id').val(this.event.id);
            $('#modal-title').val(this.event.title);
            $('#modal-service').val(this.event.service_id);
            $('#modal-employee').val(this.event.employee_id);
            $('#modal-customer').val(this.event.user_id);
            $('#modal-starts-at').val(this.event.start.format("DD-MM-YYYY HH:mm"));
            $('#modal-ends-at').val(this.event.end.format("DD-MM-YYYY HH:mm"));
            $("#eventModal .modal-title").html(this.event.title + " στις " + this.event.start.format("DD-MM-YYYY HH:mm") + " μέχρι " + this.event.end.format("HH:mm"));
            $("#eventModal").modal("show");
        }//end eventClick
        });

    // Add Event
    $('#eventForm').on('submit', function(event) {
        event.preventDefault();
        var service_id = $('#service').val();
        if (!service_id) {
            return;
        }
        var eventData = {
            service_id: service_id,
            start: $('#starts-at').val(),
            end: $('#ends-at').val(),
            user_id: $('.customer').val(),
            employee_id: $('#employee').val()
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: '/events/add',
            data: eventData,
            success: function(data) {
                if(!data.success){
                    alert(data.message);
                    alertify.error(data.reason || "Το ραντεβού δεν αποθηκεύτηκε!");
                    return;
                }
                console.log(data);
                $('#calendar').fullCalendar('renderEvent', data.event, true); // stick? = true
                alertify.success(data.message);
            },
            error: function(error) {
                alertify.error(error);
                console.log(error);
            }
        });
    });//end Add event

    // Close the Modal
    $(".dissmissEventModal").on("click", function(){
        event = {};
        $("#eventModal").modal("hide");
        $("#eventModal .modal-title").empty();
    });

    // Auto calculate the end time
    $("#modal-service, #modal-starts-at").on("change", function(ev) {
        var serviceTime = $('#modal-service>option:selected').data('time');
        var start = $('#modal-starts-at').val();
        var d = new Date(start);
        d.addMinutes(serviceTime || 0);
        var end = d.toDateTimeString();
        $('#modal-ends-at').val(end);
    });

    $("#service, #starts-at").on("change", function(ev) {
        var serviceTime = $('#service>option:selected').data('time');
        var start = $('#starts-at').val();
        var d = new Date(start);
        d.addMinutes(serviceTime || 0);
        var end = d.toDateTimeString();
        $('#ends-at').val(end);
    });


});