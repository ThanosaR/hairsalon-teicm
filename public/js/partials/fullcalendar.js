$(document).ready(function() {

    function makeDraggable(element){
        // store data so the calendar knows to render an event upon drop
        element.data('event', {
            title: $.trim(element.text()), // use the element's text as the event title
            color: element.attr("class"),
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        element.draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    }

    $('#external-events .external-event').each(function() {
        makeDraggable($(this));
    });

    $(document).on('click','.close', function(){
       $(this).parent().hide();
    });

    $("#add-new-event").click(function(){
        var txt = $("#new-event").val();
        var div = $("<div/>");
        div.addClass("external-event");
        div.addClass($(this).data("bg"));
        div.html('<i class="fa fa-window-close close" aria-hidden="true"></i>'+txt);
        $('.external-event').last().after(div);
        makeDraggable($('.external-event').last());
    });
    $(document).ready(function(){

    });
    $("[id^='calendar']").first().fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        "eventLimit":true,
        events:[
            {
                "id":null,
                "title":"Devtalk",
                "allDay":true,
                "start":"2018-01-15T00:00:00+00:00",
                "end":"2018-01-16T00:00:00+00:00",
                "color":"#ff0000",
                "url":"pass here url and any route"
            }
        ],
        editable: true,
        eventDrop: function(event, delta, revertFunc) {

            alert(event.title + " μετακινήθηκε στις " + event.start.format());

            if (!confirm("Είσαι σίγουρος για αυτή την αλλαγή;")) {
                revertFunc();
            }

        },
        droppable: true, // this allows things to be dropped onto the calendar
        drop: function(ev) {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
            console.log(ev.target,$(this))
        }

    });

    $("#color-chooser > li > a").click(function(ev){
        ev.preventDefault();
        var txtClass = $(this).attr("class");
        var bgClass = txtClass.replace("text", "bg");
        var addNewEvent = $("#add-new-event");
        var prevClass = addNewEvent.data("bg");
        addNewEvent.data("bg", bgClass);
        addNewEvent.removeClass(prevClass);
        $("#add-new-event").addClass(bgClass);
    })


});
