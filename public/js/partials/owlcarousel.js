

$('.main-slider').owlCarousel({
    autoplay:true,
    autoplayTimeout:4000,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    loop: true,
    lazyLoad: true,
    dots: false,
    margin:10,
    nav:true,
    navText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('.services').owlCarousel({
    items: 3,
    responsive: {
        0:{
            items:1
        } ,
        768: {
            items: 3,
            margin: 30
        },
        992: {
            items: 4,
            margin: 30
        }
    },
    autoplay:true,
    // autoplayTimeout:4000,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    loop: true,
    lazyLoad: true,
    dots: false,
    margin: 20,
    nav:true,
    navText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
});

