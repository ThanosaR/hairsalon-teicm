
$(document).ready(function() {

    // process the form
    $('#loginForm').submit(function(event) {
        // alert('eea');
        event.preventDefault();
            var formData = {
                'email' 	: $('input[name=email]').val(),
                'password' 	: $('input[name=password]').val()
            };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

            $.ajax({
                type:   'POST',
                url:    '/login',
                data: formData,
                encode:  true,
                success: function() {
                    $("#loginform").html("Συνδεθήκατε με επιτυχία!");
                    window.location.reload(true);
                }
            })
                .fail(function(data) {
                    $('#error-msg').empty();
                    $('#error-msg').addClass('alert alert-danger');
                    $('#error-msg').append('<div id="error-msg" role="alert"><ul><li><strong>ΠΡΟΣΟΧΗ!</strong> Λάθος δεδομένα. <p>Προσπαθήστε ξανά!</p></li></ul></div>');
                });

            // stop the form from submitting the normal way and refreshing the page
    });

});


