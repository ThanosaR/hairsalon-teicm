$(document).ready(function() {
    $(".owl-carousel").owlCarousel({
        items: 1,
        margin: 40,
        autoplay: true,
        responsive: {
           0:{
               items:1
           } ,
            768: {
               items: 3,
                margin: 30
            },
            992: {
               items: 4,
                margin: 30
            }
        }
    });
});



