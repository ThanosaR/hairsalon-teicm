@extends('layouts.app')

@section('links')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-print-1.5.1/cr-1.4.1/r-2.2.1/sl-1.2.4/datatables.min.css"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-print-1.5.1/cr-1.4.1/r-2.2.1/sl-1.2.4/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var csrf_token = $("#csrf_token").val();
            var users = {};
            var userModal = {
                defaultUser:{
                    "id":"",
                    "name":"",
                    "last_name":"",
                    "email":"",
                    "address":"",
                    "age":"",
                    "phone":"",
                    "discount":""
                },
                title: "Επεξεργασία Χρήστη",
                user: {},
                open: function(user){
                    if(!user){
                        user = JSON.parse(JSON.stringify(this.defaultUser));
                        this.title = "Δημιουργία Νέου Χρήστη";
                    }else{
                        this.title = "Επεξεργασία Χρήστη";
                    }
                    this.user = user;
                    this.render();
                    $("#userModal").modal("show");
                },
                close: function(){
                    this.user = JSON.parse(JSON.stringify(this.defaultUser));
                    $("#userModal").modal("hide");
                },
                render: function(){
                    $("#userModal .modal-title").html(this.title);
                    $("#userModalForm").empty();
                    var user = this.user;
                    var template = `<div class="form-row">
                            <div class="form-group col-md-6">
                            <input type="hidden" name="id" value="${user.id}" />
                            <input type="hidden" name="_token" value="${csrf_token}" />
                                <label for="inputCity">Όνομα</label>
                                <input type="text" name="name" value="${user.name}" class="form-control" placeholder="Όνομα Χρήστη" required />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputState">Επώνυμο</label>
                                <input type="text" class="form-control"  name="last_name" value="${user.last_name}" placeholder="Επώνυμο Χρήστη" required />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">Email</label>
                                <input type="email" class="form-control"  name="email" value="${user.email}" placeholder="Ηλεκτρονική Διεύθυνση" required />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputState">Διεύθυνση Κατοικίας</label>
                                <input type="text" class="form-control" name="address" value="${user.address}" placeholder="Διεύθυνση Κατοικίας">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">Τηλέφωνο</label>
                                <input type="text" class="form-control" name="phone" value="${user.phone}" placeholder="Κινητό / Σταθερό" required />
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputState">Ηλικία</label>
                                <input type="number" min="1" max="150" class="form-control" name="age" value="${user.age}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputZip">Έκπτωση</label>
                                <input type="te"  min="0" step="5" max="100"  class="form-control" name="discount" value="${user.discount}">
                            </div>
                        </div>`;
                    $("#userModalForm").html(template);
                },
                create: function(){
                    var formData = $("#userModalForm").serialize();
                    var modal = this;
                    $.ajax({
                        type        : 'PUT', // define the type of HTTP verb we want to use (POST for our form)
                        url         : '/admin/users/createUser', // the url where we want to POST
                        data        : formData, // our data object
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode      : true
                    })
                    // using the done promise callback
                        .done(function(data) {
                            if(data.success){
                                userTable.ajax.reload();
                                alertify.success(data.message);
                                modal.close();
                            }else{
                                var err = data.message;
                                if(Object.keys(data.errors).length === 1){
                                    err = data.errors[Object.keys(data.errors)[0]][0];
                                }
                                alertify.error(err);
                                for(var x in data.errors){
                                    var element = $("#userModalForm")[0].elements.namedItem(x);
                                    $(element).parent(".form-group").first().addClass("has-error");
                                    $(element).one("focus", function(){
                                        modal.clearError(this);
                                    });
                                }
                                setTimeout(function(){
                                    modal.clearErrors();
                                }, 10000);
                            }
                        })
                        .fail(function(err){
                            console.log(err);
                            alertify.error(err);
                            modal.close();
                        });
                },
                update: function(){
                    var formData = $("#userModalForm").serialize();
                    var modal = this;
                    $.ajax({
                        type        : 'PUT', // define the type of HTTP verb we want to use (POST for our form)
                        url         : '/admin/users/updateUser', // the url where we want to POST
                        data        : formData, // our data object
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode      : true
                    })
                    // using the done promise callback
                        .done(function(data) {
                            if(data.success){
                                userTable.ajax.reload();
                                alertify.success(data.message);
                                modal.close();
                            }else{
                                var err = data.message;
                                if(Object.keys(data.errors).length === 1){
                                    err = data.errors[Object.keys(data.errors)[0]][0];
                                }
                                alertify.error(err);
                                for(var x in data.errors){
                                    var element = $("#userModalForm")[0].elements.namedItem(x);
                                    $(element).parent(".form-group").first().addClass("has-error");
                                    $(element).one("focus", function(){
                                        modal.clearError(this);
                                    });
                                }
                                setTimeout(function(){
                                    modal.clearErrors();
                                }, 10000);
                            }
                        })
                        .fail(function(err){
                            console.log(err);
                            alertify.error(err);
                            modal.close();
                        });
                },
                save: function(){
                    if(this.user.id){
                        this.update();
                    }else{
                        this.create();
                    }
                },
                clearErrors: function(){
                    $.each($("#userModalForm")[0].elements, function(index, elem){
                        $(elem).parent(".form-group").first().removeClass("has-error");
                    });
                },
                clearError: function(elem){
                    $(elem).parent(".form-group").first().removeClass("has-error");
                },
                init: function(){
                    $('#userModal').on("click", ".close-btn", function(){
                        userModal.close();
                    });
                    $('#userModalFormSubmit').on("click", function(ev){
                        ev.preventDefault();
                        console.log("save");
                        userModal.save();
                    });
                }
            };
            userModal.init();
            var userTable = $('#usersDatatable').DataTable({
                responsive: true,
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-right'f><'col-sm-4 text-right'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons:[
                    {
                        text: 'Δημιουργία Χρήστη',
                        className: 'btn btn-info',
                        action: function (e, node, config){
                            userModal.open();
                        }
                        // action: function ( e, dt, node, config ) {
                        //     alert( 'Button activated' );
                        // }
                    }
                ],
                ajax: {
                    url: '{{ route('users/getUsers') }}',
                    dataSrc: function(json){
                        for(var x = 0; x < json.data.length; x++){
                            users[json.data[x].id] = json.data[x];
                        }
                        return json.data;
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email'},
                    {data: 'address', name: 'address'},
                    {data: 'age', name: 'age'},
                    {data: 'phone', name: 'phone'},
                    {data: 'discount', name: 'discount'},
                    {data: 'created_at', name: 'created_at'},
                    {data: function ( row, type, val, meta ) {
                            if(!row.id)
                                return null;
                            return "<div class='btn-group'>" +
                                "<button class='btn btn-warning btn-xs btn-block' data-edit-id='"+row.id+"'>Edit</button>" +
                                // "<a href='/admin/users/"+row.id+"/edit' class='btn btn-warning btn-xs btn-block'>Edit</a>" +
                                "<button class='btn btn-danger btn-xs btn-block' data-delete-id='"+row.id+"'>Delete</button>"+
                                "</div>";
                        }
                    }

                ]
            });

            // userTable.buttons().container()
            //     .appendTo( $('.col-sm-6:eq(1)', userTable.table().container() ) );
            $('#usersDatatable').on("click", "[data-delete-id]", function(){
                var user_id = $(this).data("delete-id");
                if(!confirm("Είσαι σίγουρος ότι θέλεις να διαγράψεις αυτόν τον χρήστη?")){
                    return;
                }
                $.ajax({
                    type        : 'DELETE', // define the type of HTTP verb we want to use (POST for our form)
                    url         : '/admin/users/delete', // the url where we want to POST
                    data        : {
                        _token: csrf_token,
                        user_id : user_id
                    }, // our data object
                    dataType    : 'json', // what type of data do we expect back from the server
                    encode      : true
                })
                // using the done promise callback
                    .done(function(data) {
                        if(data.success){
                            userTable.ajax.reload();
                            alertify.success(data.message);
                        }else{
                            alertify.error(data.message);
                        }
                    })
                    .fail(function(err){
                        console.log(err);
                    });
            });



            $('#usersDatatable').on("click", "[data-edit-id]", function(){
                var user_id = $(this).data("edit-id");
                userModal.open(users[user_id])
            });
        });

    </script>
@endsection

@section('main-content')

    <div class="panel panel-default">
        <div class="panel-heading text-center"><h4>Οι Χρήστες του Hairsalon</h4></div>

        <div class="panel-body">
            <table class="table table-bordered table-striped" id="usersDatatable" style="width:100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Όνομα</th>
                    <th>Επίθετο</th>
                    <th>Email</th>
                    <th>Διεύθυνση</th>
                    <th>Ηλικία</th>
                    <th>Τηλέφωνο</th>
                    <th>Έκπτωση</th>
                    <th>Ημερομηνία Εγγραφής</th>
                    <th>Ενέργειες</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="modal fade" id="userModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-btn">&times;</button>
                    <h4 class="modal-title">Προσθήκη Χρήστη.</h4>
                </div>
                <div class="modal-body">
                    <form class="container-fluid" id="userModalForm">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="userModalFormSubmit">Αποθήκευση</button>
                    <button type="button" class="btn btn-default close-btn">Κλείσιμο</button>
                </div>
            </div>

        </div>
    </div>
@endsection