@extends('layouts.app')
@section('links')
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.min.css">
    <link rel="stylesheet" media="print" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.print.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('scripts')


    <script src="{!! asset("plugins/jquery-ui.min.js") !!}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/locale/el.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{!! asset("js/partials/fullcalendar2.js") !!}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>



@endsection
@section('main-content')
       <div class="row">
        <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h4 class="box-title"> Δημιουργία Ραντεβού</h4>
                </div>
                <div id="newEvent" name="service" class="box-body">
                    <form id="eventForm">
                        <div id="eventFormCopy">
                            <div class="form-group">
                                <label for="service">Υπηρεσία</label>
                                <select class="form-control" id="service" required="required">
                                    <option value="">Επιλέξτε μια υπηρεσία</option>
                                    @foreach($services as $service)
                                        <option value="{{$service["id"]}}" data-time="{{$service["time"]}}">{{$service["type"]}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="employee">Όνομα Υπαλλήλου</label>
                                <select class="form-control" id="employee" required="required">
                                    <option value="">Επιλέξτε Υπάλληλο</option>
                                    @foreach($employees as $employee)
                                        <option value="{{$employee["id"]}}">{{$employee["name"]}} {{$employee["last_name"]}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="customer">Πελάτης</label>
                                <select class="form-control" id="customer" required="required">
                                    <option value="">Επιλέξτε Πελάτη</option>
                                    @foreach($users as $user)
                                        <option value="{{$user["id"]}}">{{$user["name"]}} {{$user["last_name"]}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="start-at">Ημέρα, Ώρα έναρξης</label>
                                <input id="starts-at" class="form-control" type="text" name="daterange"  placeholder="Επιλέξτε μια ημερομηνία" required="required">
                            </div>
                            <div class="form-group">
                                <label for="end-at">Ημέρα, Ώρα λήξης</label>
                                <input id="ends-at" disabled class="form-control" type="text" name="daterange" placeholder="Επιλέξτε μια ημερομηνία λήξης" required="required">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Δημιουργία</button>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /. box -->

        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    <div id="eventModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close dissmissEventModal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="modal-id" name="id" value="" />
                    <div class="form-group">
                        <label for="modal-service">Υπηρεσία</label>
                        <select class="form-control" id="modal-service" required="required">
                            <option value="">Επιλέξτε μια υπηρεσία</option>
                            @foreach($services as $service)
                                <option value="{{$service["id"]}}" data-time="{{$service["time"]}}">{{$service["type"]}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="modalEmployee">Όνομα Υπαλλήλου</label>
                        <select class="form-control" id="modal-employee" required="required">
                            <option value="">Επιλέξτε Υπάλληλο</option>
                            @foreach($employees as $employee)
                                <option value="{{$employee["id"]}}">{{$employee["name"]}} {{$employee["last_name"]}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="modalCustomer">Πελάτης</label>
                        <select class="form-control" id="modal-customer" required="required">
                            <option value="">Επιλέξτε Πελάτη</option>
                            @foreach($users as $user)
                                <option value="{{$user["id"]}}">{{$user["name"]}} {{$user["last_name"]}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="modal-starts-at">Ημέρα, Ώρα έναρξης</label>
                        <input id="modal-starts-at" class="form-control" type="text" name="daterange"  placeholder="Επιλέξτε μια ημερομηνία" required="required">
                    </div>
                    <div class="form-group">
                        <label for="modal-ends-at">Ημέρα, Ώρα λήξης</label>
                        <input id="modal-ends-at" disabled class="form-control" type="text" name="daterange" placeholder="Επιλέξτε μια ημερομηνία λήξης" required="required">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary saveEventModal">Ενημέρωση</button>
                    <button type="button" class="btn btn-danger deleteEventModal">Διαγραφή</button>
                    <button type="button" class="btn btn-default dissmissEventModal">Κλείσιμο</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
