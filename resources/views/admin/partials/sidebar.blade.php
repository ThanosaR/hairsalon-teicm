<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>

                </div>
                <div class="signout text-right">
                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat btn-danger" id="logout"
                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                       <span style="color: white;"> {{ trans('adminlte_lang::message.signout') }}</span>
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        <input type="submit" value="logout" style="display: none;">
                    </form>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->

        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url('admin') }}"><i class='fa fa-home'></i> <span>{{ trans('adminlte_lang::message.home') }}</span></a></li>
            <li><a href="/admin/events"><i class='fa fa-calendar'></i>Ραντεβού</a></li>
            <li><a href="/admin/users"><i class='fa fa-users'></i>Χρήστες</a></li>
            <li><a href="/admin/services"><i class='fa fa-shopping-bag'></i>Υπηρεσίες</a></li>
            {{--@if (Auth::user()->can('root_panel'))--}}
            {{--<li><a href="/admin/roots"><i class='fa fa-lock'></i>Δικαιώματα</a></li>--}}
            {{--@endif--}}
            {{--<li><a href="#"><i class='fa fa-share-alt-square'></i>Social Media</a></li>--}}
            <li><a href="/admin/info"><i class='fa fa-info'></i>Πληροφορίες Πτυχιακής</a></li>
            {{--<li><a href="#"><i class='fa fa-link'></i> <span>{{ trans('adminlte_lang::message.anotherlink') }}</span></a></li>--}}
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class='fa fa-link'></i> <span>{{ trans('adminlte_lang::message.multilevel') }}</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="#">{{ trans('adminlte_lang::message.linklevel2') }}</a></li>--}}
                    {{--<li><a href="#">{{ trans('adminlte_lang::message.linklevel2') }}</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
