@extends('layouts.app')

@section('links')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-print-1.5.1/cr-1.4.1/r-2.2.1/sl-1.2.4/datatables.min.css"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/b-1.5.1/b-colvis-1.5.1/b-print-1.5.1/cr-1.4.1/r-2.2.1/sl-1.2.4/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var csrf_token = $("#csrf_token").val();
            var services = {};
            var serviceModal = {
                defaultService:{
                    "id": "",
                    "type":"",
                    "time":"",
                    "cost":""
                },
                title: "Επεξεργασία Υπηρεσίας",
                service: {},
                open: function(service){
                    if(!service){
                        service = JSON.parse(JSON.stringify(this.defaultService));
                        this.title = "Δημιουργία Νέας Υπηρεσίας";
                    }else{
                        this.title = "Επεξεργασία Υπηρεσίας";
                    }
                    this.service = service;
                    this.render();
                    $("#serviceModal").modal("show");
                },
                close: function(){
                    this.service = JSON.parse(JSON.stringify(this.defaultService));
                    $("#serviceModal").modal("hide");
                },
                render: function(){
                    $("#serviceModal .modal-title").html(this.title);
                    $("#serviceModalForm").empty();
                    var service = this.service;
                    var template = `<div class="form-row">
                            <div class="form-group col-md-6">
                            <input type="hidden" name="id" value="${service.id}" />
                            <input type="hidden" name="_token" value="${csrf_token}" />
                                <label for="inputCity">Τύπος</label>
                                <input type="text" name="type" value="${service.type}" class="form-control" placeholder="Τύπος Υπηρεσίας" required />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputState">Διάρκεια</label>
                                <input type="number" class="form-control"  name="time" value="${service.time}" placeholder="Διάρκεια (Λεπτά)" required />
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                                <label for="inputCity">Κόστος</label>
                                <input type="number" class="form-control"  name="cost" value="${service.cost}" placeholder="Τιμή" required />
                        </div>
                        </div>`;
                    $("#serviceModalForm").html(template);
                },
                create: function(){
                    var formData = $("#serviceModalForm").serialize();
                    var modal = this;
                    $.ajax({
                        type        : 'PUT', // define the type of HTTP verb we want to use (POST for our form)
                        url         : '/admin/services/createService', // the url where we want to POST
                        data        : formData, // our data object
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode      : true
                    })
                    // using the done promise callback
                        .done(function(data) {
                            if(data.success){
                                serviceTable.ajax.reload();
                                alertify.success(data.message);
                                modal.close();
                            }else{
                                var err = data.message;
                                if(Object.keys(data.errors).length === 1){
                                    err = data.errors[Object.keys(data.errors)[0]][0];
                                }
                                alertify.error(err);
                                for(var x in data.errors){
                                    var element = $("#serviceModalForm")[0].elements.namedItem(x);
                                    $(element).parent(".form-group").first().addClass("has-error");
                                    $(element).one("focus", function(){
                                        modal.clearError(this);
                                    });
                                }
                                setTimeout(function(){
                                    modal.clearErrors();
                                }, 10000);
                            }
                        })
                        .fail(function(err){
                            console.log(err);
                            alertify.error(err);
                            modal.close();
                        });
                },
                update: function(){
                    var formData = $("#serviceModalForm").serialize();
                    var modal = this;
                    $.ajax({
                        type        : 'PUT', // define the type of HTTP verb we want to use (POST for our form)
                        url         : '/admin/services/updateService', // the url where we want to POST
                        data        : formData, // our data object
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode      : true
                    })
                    // using the done promise callback
                        .done(function(data) {
                            if(data.success){
                                serviceTable.ajax.reload();
                                alertify.success(data.message);
                                modal.close();
                            }else{
                                var err = data.message;
                                if(Object.keys(data.errors).length === 1){
                                    err = data.errors[Object.keys(data.errors)[0]][0];
                                }
                                alertify.error(err);
                                for(var x in data.errors){
                                    var element = $("#serviceModalForm")[0].elements.namedItem(x);
                                    $(element).parent(".form-group").first().addClass("has-error");
                                    $(element).one("focus", function(){
                                        modal.clearError(this);
                                    });
                                }
                                setTimeout(function(){
                                    modal.clearErrors();
                                }, 10000);
                            }
                        })
                        .fail(function(err){
                            console.log(err);
                            alertify.error(err);
                            modal.close();
                        });
                },
                save: function(){
                    if(this.service.id){
                        this.update();
                    }else{
                        this.create();
                    }
                },
                clearErrors: function(){
                    $.each($("#serviceModalForm")[0].elements, function(index, elem){
                        $(elem).parent(".form-group").first().removeClass("has-error");
                    });
                },
                clearError: function(elem){
                    $(elem).parent(".form-group").first().removeClass("has-error");
                },
                init: function(){
                    $('#serviceModalForm').on("click", ".close-btn", function(){
                        serviceModal.close();
                    });
                    $('#serviceModalFormSubmit').on("click", function(ev){
                        ev.preventDefault();
                        console.log("save");
                        serviceModal.save();
                    });
                }
            };
            serviceModal.init();

            var serviceTable = $('#servicesDatatable').DataTable({
                responsive: true,
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-right'f><'col-sm-4 text-right'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons:[
                    {
                        text: 'Δημιουργία Υπηρεσίας',
                        className: 'btn btn-info',
                        action: function ( e, dt, node, config ) {
                            serviceModal.open();
                        }
                    }
                ],
                ajax: {
                    url: '{{ route('services/getServices') }}',
                    dataSrc: function(json){
                        for(var x = 0; x < json.data.length; x++){
                            services[json.data[x].id] = json.data[x];
                        }
                        return json.data;
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'type', name: 'type'},
                    {data: 'time', name: 'time'},
                    {data: 'cost', name: 'cost'},
                    {data: 'created_at', name: 'created_at'},
                    {data: function ( row, type, val, meta ) {
                            if(!row.id)
                                return null;
                            return "<div class='btn-group'>" +
                                "<button class='btn btn-warning btn-xs btn-block' data-edit-id='"+row.id+"'>Edit</button>" +
                                // "<a href='/admin/services/"+row.id+"/edit' class='btn btn-warning btn-xs btn-block'>Edit</a>" +
                                "<button class='btn btn-danger btn-xs btn-block' data-delete-id='"+row.id+"'>Delete</button>"+
                                "</div>";
                        }
                    }

                ]
            });

            // serviceTable.buttons().container()
            //     .appendTo( $('.col-sm-6:eq(1)', serviceTable.table().container() ) );
            $('#servicesDatatable').on("click", "[data-delete-id]", function(){
                var service_id = $(this).data("delete-id");
                if(!confirm("Είσαι σίγουρος ότι θέλεις να διαγράψεις αυτήν την υπηρεσία;")){
                    return;
                }
                $.ajax({
                    type        : 'DELETE',
                    url         : '/admin/services/delete',
                    data        : {
                        _token: csrf_token,
                        service_id : service_id
                    }, // our data object
                    dataType    : 'json',
                    encode      : true
                })
                    .done(function(data) {
                        if(data.success){
                            serviceTable.ajax.reload();
                        }
                    })
                    .fail(function(err){
                        console.log(err);
                    });
            });

            $('#servicesDatatable').on("click", "[data-edit-id]", function(){
                var service_id = $(this).data("edit-id");
                serviceModal.open(services[service_id])
            });


        });

    </script>
@endsection

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading text-center"><h4>Οι Υπηρεσίες του Hairsalon</h4></div>

        <div class="panel-body">
            <table class="table table-bordered table-striped" id="servicesDatatable" style="width:100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Τύπος</th>
                    <th>Διάρκεια (Λεπτά)</th>
                    <th>Τιμή</th>
                    <th>Ημερομηνία Εγγραφής</th>
                    <th>Ενέργειες</th>
                </tr>

                </thead>
            </table>
        </div>
    </div>

    <div class="modal fade" id="serviceModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-btn" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Προσθήκη Υπηρεσίας.</h4>
                </div>
                <div class="modal-body">
                    <form class="container-fluid" id="serviceModalForm">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="serviceModalFormSubmit">Αποθήκευση</button>
                    <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Κλείσιμο</button>
                </div>
            </div>

        </div>
    </div>
@endsection
