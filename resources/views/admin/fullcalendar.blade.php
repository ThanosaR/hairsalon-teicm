@extends('layouts.app')
@section('links')
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.min.css">
    <link rel="stylesheet" media="print" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.print.min.css">
@endsection
@section('scripts')
    <script src="{!! asset("plugins/jquery-ui.min.js") !!}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/locale/el.js"></script>
    {{--{!! $calendar->script() !!}--}}

    <script src="{!! asset("js/partials/fullcalendar.js") !!}"></script>
@endsection
@section('main-content')
       <div class="row">
        <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h4 class="box-title">Draggable Events</h4>
                </div>
                <div class="box-body">
                    <!-- the events -->
                    <div id="external-events">
                        <div class="external-event bg-green"><i class="fa fa-window-close close" aria-hidden="true"></i>Αντρικό Κούρεμα </div>
                        <div class="external-event bg-light-blue"><i class="fa fa-window-close close" aria-hidden="true"></i>Παιδικό Κούρεμα</div>
                        <div class="external-event bg-red"><i class="fa fa-window-close close" aria-hidden="true"></i>Γυναικείο Κούρεμα</div>
                        <div class="external-event bg-yellow"><i class="fa fa-window-close close" aria-hidden="true"></i>Χτένισμα</div>
                        <div class="external-event bg-aqua"><i class="fa fa-window-close close" aria-hidden="true"></i>Βάψιμο</div>
                        <div class="external-event bg-light-blue"><i class="fa fa-window-close close" aria-hidden="true"></i>Λούσιμο</div>
                        <div class="external-event bg-red"><i class="fa fa-window-close close" aria-hidden="true"></i>Φρεσκάρισμα</div>
                        <div class="external-event bg-black"><i class="fa fa-window-close close" aria-hidden="true"></i>Θεραπεία Μαλλιών</div>
                        <div class="external-event bg-green"><i class="fa fa-window-close close" aria-hidden="true"></i>Stylling</div>
                        <div class="checkbox">
                            <label for="drop-remove">
                                <input type="checkbox" id="drop-remove">
                                remove after drop
                            </label>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Event</h3>
                </div>
                <div class="box-body">
                    <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                        <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                        <ul class="fc-color-picker" id="color-chooser">
                            <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                            <li><a class="text-black" href="#"><i class="fa fa-square"></i></a></li>
                        </ul>
                    </div><!-- /btn-group -->
                    <div class="input-group">
                        <input id="new-event" type="text" class="form-control" placeholder="Event Title">
                        <div class="input-group-btn">
                            <button id="add-new-event" type="button" data-bg="bg-aqua" class="btn bg-aqua btn-flat">Add</button>
                        </div><!-- /btn-group -->
                    </div><!-- /input-group -->
                </div>
            </div>
        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    {{--<div id="calendar"></div>--}}
                    {!! $calendar->calendar() !!}
                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div><!-- /.row -->


@endsection
