

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="{{ asset('css/homepage.css') }}" rel="stylesheet">

<div class="container">

    <div class="jumbotron">
        <div class="text-center"><img src="/img/success-icon.png"></div>
        <h2 class="display-4 text-center">Ολοκλήρωση Εγγραφής!</h2>
        {{--<p class="lead">Η Εγγραφή σας στο Hairsalon Ολοκληρώθηκε με επιτυχία! Σας ευχαριστούμε πολύ...</p>--}}
        <hr class="my-4 ">
        <p class="text-center">Η εγγραφή σας στο Hairsalon Ολοκληρώθηκε με επιτυχία. Σας ευχαριστούμε πολύ...</p>
        <p class="">
            <a class="btn btn-primary btn-lg" href="/" role="button">Επιστροφή στην Αρχική!</a>
        </p>
    </div>
</div>