@extends('layouts.front')

@section('main-content')
<!-- Owl Carousel  -->

<div class="owl-container">
    <div class="owl-carousel owl-theme main-slider">
        <div class="item" style="background:url('/img/owl-carousel/img-carousel-4.jpg')"></div>
        <div class="item" style="background:url('/img/owl-carousel/img-carousel.jpg')"></div>
        <div class="item" style="background:url('/img/owl-carousel/img-carousel-3.jpg')"></div>
        <div class="item" style="background:url('/img/owl-carousel/img1.jpg')"></div>
        <div class="item" style="background:url('/img/owl-carousel/img2.jpg')"></div>
    </div>
</div>

<!-- Services -->
<section id="online">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Κλείσε <strong>Online</strong> ραντεβού!</h2><hr>
                <h3 class="section-subheading text-mute">Κλείσε ραντεβού στο κομμωτήριο μας, μόνο με 3 βήματα</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
            </span>
                <h4 class="service-heading">Κάνε Σύνδεση</h4>
                <p class="text-mute">Πρώτο βήμα, κάνε εγγραφή ή σύνδεση με τον λογαριασμού σου, για να δεις όλες τις υπηρεσίες μας.</p>
            </div>
            <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-calendar-check-o fa-stack-1x fa-inverse"></i>
            </span>
                <h4 class="service-heading">Ψάξε Ημερομηνία</h4>
                <p class="text-mute">Ψάξε την διαθέσιμη ημερομηνία, σε συδυασμό με την υπηρεσία και τον υπάλληλο που επιθυμείς.</p>
            </div>
            <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-cut fa-stack-1x fa-inverse"></i>
            </span>
                <h4 class="service-heading">Κάνε Κράτηση</h4>
                <p class="text-mute">Αυτό ήταν. Πατάς Δημιουργία και το ρεντεβού σου είναι έτοιμο.</p>
            </div>
        </div>
    </div>
</section>

<!-- slider-cards - services -->
<section id="slider-cards" class="section-full">
    <div class="container">
        <div class="active-testimonial-carousel">
            <div class="single-testimonial">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="product-area-title text-center">
                            <h2 class="text-uppercase">Online κρατηση</h2><hr>
                            <p class="h2">Όλες οι διαθέσιμες υπηρεσίες <br>για να κάνεις  <strong>Κράτηση</strong> με λίγα μόνο <br>κλικ!</p>
                        </div>
                    </div>
                </div>
                <div class="owl-carousel services">

                    @foreach($services as $service)
                    <div class="col-md-13">
                        <div class="single-pricing-table">
                            <div class="top">
                                <div class="head text-center">
                                    <span class="lnr lnr-apartment"></span>
                                    <h4 class="text-white">{{$service["type"]}}</h4>
                                    <i class="fa fa-clock-o text-white" aria-hidden="true"> {{$service["time"]}}'</i>
                                </div>
                                <div class="package text-center">
                                    <div class="price"><strong>{{$service["cost"]}}€</strong></div>
                                    <span class="text-white fpa"><em>Στην τιμή συμπεριλαμβάνεται ο ΦΠΑ. </em></span>
                                </div>
                            </div>
                            <div class="bottom text-center">
                                <a href="/events" class="primary-btn reservation text-uppercase d-inline-flex align-items-center">Κρατηση<span class="lnr lnr-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Pricing Area -->


<!-- Hairsalon's Staff -->
<section id="staff" class="testimonials text-center bg-light staff">
    <div class="container">
        <h2 class="mb-5 text-uppercase">Το προσωπικο μας</h2><hr>
    </div>
    <div class="container staff">
       <h4> <p>Εδώ θα βρείτε όλο το προσωπικό του Hairsalon!</p></h4>
        <p> Το Hairsalon είναι ένα νέο σύγχρονο κομμωτήριο, στο οποίο ο πελάτης έχει τη δυνατότητα να κλείσει ραντεβού για την υπηρεσία που θέλει, γρήγορα και εύκολα, <strong>Online</strong> από τη φορητή του συσκευή. Φυσικά μέσω της εφαρμογής μπορεί να επιλέξει και συγκεκριμένο υπάλληλο που θα αναλάβει την περιποίηση του. Παρακάτω μπορείτε να γνωρίσετε το προσωπικό μας! </p>
        <br>
        <div class="row">
            <div class="col-lg-4 staff-item">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/staff/boy-1.png" alt="Linus Torvalds">
                    <h5>Αντώνης Αντωνίου</h5>
                    <p class="font-weight-light mb-0">"Βρίσκεται στο κατάστημα μας, 4 μήνες."</p>
                </div>
            </div>
            <div class="col-lg-4 staff-item">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/staff/girl.png" alt="Mark Zuckerberg">
                    <h5>Νίκη Ζαγόρη</h5>
                    <p class="font-weight-light mb-0">"Διαθέσιμη για όλες τις υπηρεσίες. Είναι μαζί μας εδώ και 5 μήνες."</p>
                </div>
            </div>
            <div class="col-lg-4 staff-item">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/staff/girl-1.png" alt="Jennifer Lawrence">
                    <h5>Μαρία Μαράκη</h5>
                    <p class="font-weight-light mb-0">"Με ειδικότητα κυρίως στα παιδικά κουρέματα."</p>
                </div>
            </div>
            <div class="col-lg-4 staff-item">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/staff/man.png" alt="Bill Gates">
                    <h5>Γιάννης Ιωάννου</h5>
                    <p class="font-weight-light mb-0">"Υπεύθυνος κυρίως για τις γυναικείες υπηρεσίες."</p>
                </div>
            </div>
            <div class="col-lg-4 staff-item">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/staff/man-1.png" alt="Angelina Jolie">
                    <h5>Νίκος Νικολάου</h5>
                    <p class="font-weight-light mb-0">"Το πιο παλιό μέλος τους καταστήματος μας. Διαθέσιμος σε όλες τις υπηρεσίες"</p>
                </div>
            </div>
            <div class="col-lg-4 staff-item">
                <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img class="img-fluid rounded-circle mb-3" src="img/staff/man-2.png" alt="Sundar Pichai">
                    <h5>Γιώργος Γεωργίου</h5>
                    <p class="font-weight-light mb-0">"Αποτελεί την πιο πρόσφατη προσθήκη της ομάδας μας."</p>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<!-- Download App -->--}}
{{--<section id="app" class="cta">--}}
    {{--<div class="cta-content">--}}
        {{--<div class="container">--}}
            {{--<h2>Κατέβασε την εφαρμογή μας.</h2>--}}
            {{--<div class="badges">--}}
                {{--<a class="badge-link" href="#"><img src="img/google-play.png" alt=""></a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="overlay"></div>--}}
{{--</section>--}}

<!-- About -->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Σχετικα με μας</h2><hr>
                <h3 class="section-subheading text-muted pb-5">Η πορεία του Hairsalon από το 2014 μέχρι και σήμερα...</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    {{--<li>--}}
                        {{--<div class="timeline-image">--}}
                            {{--<img class="rounded-circle img-fluid" src="https://www.daz3d.com/media/catalog/product/cache/1/thumbnail/689303033aebc8cae535000c73c8db4b/0/1/01-chez-lucille-hair-salon-daz3d.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="timeline-panel">--}}
                            {{--<div class="timeline-heading">--}}
                                {{--<h4>2014</h4>--}}
                                {{--<h4 class="subheading">Πρώτο ξεκίνιμα</h4>--}}
                            {{--</div>--}}
                            {{--<div class="timeline-body">--}}
                                {{--<p class="text-mute">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="timeline-inverted">--}}
                        {{--<div class="timeline-image">--}}
                            {{--<img class="rounded-circle img-fluid" src="https://igx.4sqi.net/img/general/200x200/230245_H3Ywcah4t02ENzH3_APou7hc53qWkVpo6ofh_iqk8sI.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="timeline-panel">--}}
                            {{--<div class="timeline-heading">--}}
                                {{--<h4>Μάρτιος 2015</h4>--}}
                                {{--<h4 class="subheading">Νέο κατάστημα</h4>--}}
                            {{--</div>--}}
                            {{--<div class="timeline-body">--}}
                                {{--<p class="text-mute">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/hairsalon-logo.png" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>Νοέμβριος 2014</h4>
                                <h4 class="subheading">Δημιουργία της εφαρμογής</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-mute">Έγινε η δημιουργία και η κυκλοφορία της πρώτης έκδοσης, της Web based εφαρμογή μας!</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4 class="text-muted">Γίνε και εσύ
                                <br>μέρος της
                                <br>Ιστορίας μας!</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<div id="googleMap">
    <iframe width="100%" height="400" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/view?zoom=15&center=41.075608, 23.553593&key=AIzaSyCcajqBNI4y5V0cmLp-JaY7fWIaUfB184Y" allowfullscreen></iframe>
</div>

@endsection