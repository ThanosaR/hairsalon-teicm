
@if (Auth::guest())
    <nav id="navi" class="navbar navbar-toggleable-md navbar-light bg-faded fixed-top">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand text-white js-scroll-trigger" href="#">Hair Salon</a>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-item nav-link active ml-auto" href="#" style="color: #fed136">Αρχική <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link" href="#slider-cards" style="color: #fed136">Υπηρεσίες</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link" href="#staff" style="color: #fed136">Προσωπικό</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link" href="#about" style="color: #fed136">Σχετικά</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link" href="#googleMap" style="color: #fed136">Βρείτε μας</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" data-toggle="modal" data-target="#LoginModal" style="color: white">Σύνδεση</a>
                </li>
            </ul>
        </div>
    </nav>
@else
    <nav id="navi" class="navbar navbar-toggleable-md navbar-light bg-faded fixed-top">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand text-white js-scroll-trigger" href="#">Hair Salon</a>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-item nav-link active ml-auto" href="#" style="color: #fed136">Αρχική <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link" href="#slider-cards" style="color: #fed136">Υπηρεσίες</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link" href="#staff" style="color: #fed136">Προσωπικό</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link" href="#about" style="color: #fed136">Σχετικά</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link" href="#googleMap" style="color: #fed136">Βρείτε μας</a>
                </li>

                <div class="dropdown show">
                    <a class="btn btn-danger dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        Γειά σου, <span class="hidden-xs">{{ Auth::user()->name }}!</span>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="/events">Ραντεβού</a>
                        <a class="dropdown-item btn" data-toggle="modal" data-target="#PersonalDetail">Στοιχεία</a>
                        {{--<a class="dropdown-item btn" data-toggle="modal" data-target="#PersonalPassword">Αλλαγή Κωδικού</a>--}}
                        @if( Auth::user()->can('admin_panel') )
                            <a class="dropdown-item" href="/admin">Admin Panel</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="btn btn-primary" href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout" style="width: 100%;"
                           onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            Αποσύνδεση
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            <input type="submit" value="logout" style="display: none;">
                        </form>
                    </div>
                </div>
            </ul>
        </div>


        </div>
    </nav>
@endif
