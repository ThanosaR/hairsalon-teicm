<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@include('layouts.partials.htmlheader')

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="skin-blue sidebar-mini">
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.8/angular.min.js">
    // angular.module("hairsalon", [])
        //     .controller("UsersController", UsersController);
        //
        // UsersController.$inject = ["$scope"];
        //
        // function UsersController($scope){
        //     $scope.alertUser = function(){
        //         alert("Hello world");
        //     }
        // }
    </script>--}}

    <div id="app" v-cloak ng-app="hairsalon">
        <div class="wrapper">

        @include('layouts.partials.mainheader')

        @include('admin.partials.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @include('layouts.partials.contentheader')

            <!-- Main content -->
            <section class="content">
                <input id="csrf_token" type="hidden" name="_token" value="{{ csrf_token() }}">
                <!-- Your Page Content Here -->
                @yield('main-content')
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        @include('layouts.partials.controlsidebar')

        @include('layouts.partials.footer')

        </div><!-- ./wrapper -->
    </div>
@include('layouts.partials.scripts')
@yield('scripts')


</body>
</html>
