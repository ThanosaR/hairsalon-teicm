<html lang="en">
<head>

    <title>HairSalon - TEICM</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- fav icon -->
    <link rel="shortcut icon" href="/img/fav-icon.png" type="image/x-icon">
    <link rel="manifest" href="/manifest.json">
    @include('layouts.partials.css_front')

</head>

{{--<body ng-app="myApp" ng-controller="Ctrlit" id="start" data-spy="scroll" data-target=".navbar" data-offset="50">--}}
<body>

<!-- Navigation Menu -->

@include('layouts.partials.navHome')

<!-- END Navigation Menu -->

<!-- The Login Modal -->
<div class="modal fade" id="LoginModal">
    <div class="modal-dialog">
        <!-- Modal body -->
        @include('auth.login')
    </div>
</div>
<!-- END the Login Modal -->

<div class="modal fade" id="PersonalPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Αλλαγή Κωδικού</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if (Auth::user())
                    <form class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" id="id" placeholder="Όνομα" value="{{ Auth::user()->id }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">Παλιός Κωδικός</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="oldPassword" placeholder="Παλιός Κωδικός" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="control-label">Νέος Κωδικός</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="newPassword" placeholder="Νέος Κωδικός" value="">
                            </div>
                        </div>
                    </form>
                @endif


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Κλείσιμο</button>
                <button type="button" class="btn btn-success" onclick="updatePassword()" data-dismiss="modal">Ενημέρωση</button>
            </div>
        </div>
    </div>
</div>
<!-- END the Personal password Modal -->

<!-- The Personal Details Modal -->
<div class="modal fade" id="PersonalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Προσωπικά Στοιχεία</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if (Auth::user())
                    <form class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" id="userId" placeholder="Όνομα" value="{{ Auth::user()->id }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Όνομα</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="userName" placeholder="Όνομα" value="{{ Auth::user()->name }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Επίθετο</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="userLastName" placeholder="Επίθετο" value="{{ Auth::user()->last_name }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="userEmail" placeholder="Email" value="{{ Auth::user()->email }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Διεύθυνση</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="userAddress" placeholder="Διεύθυνση" value="{{ Auth::user()->address }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Έκπτωση</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="userDiscount" placeholder="Έκπτωση" value="{{ Auth::user()->discount }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Ηλικία</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="userAge" placeholder="Ηλικία" value="{{ Auth::user()->age }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Τηλέφωνο</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="userPhone" placeholder="Τηλέφωνο" value="{{ Auth::user()->phone }}" disabled>
                            </div>
                        </div>
                    </form>
                @endif


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Κλείσιμο</button>
                <button type="button" class="btn btn-primary" onclick="enable()">Επεξεργασία</button>
                <button type="button" class="btn btn-success" onclick="updateUser()" data-dismiss="modal">Ενημέρωση</button>
            </div>
        </div>
    </div>
</div>
<!-- END the Personal Details Modal -->
<script>
    function updateUser() {
        var formData = {
            id: $('#userId').val(),
            name: $('#userName').val(),
            last_name: $('#userLastName').val(),
            email: $('#userEmail').val(),
            address: $('#userAddress').val(),
            password: $('#userPassword').val(),
            age: $('#userAge').val(),
            phone: $('#userPhone').val(),
            discount: $('#userDiscount').val(),
        };

        $.ajax({
            type        : 'PUT', // define the type of HTTP verb we want to use (POST for our form)
            url         : '/updateUser', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

            .done(function(data) {
                if(data.success){
                    alertify.success(data.message);
                }else{
                    alert('fail');
                    }
            })
            .fail(function(err){
                console.log(err);
            });
    }
</script>

<script>
    function enable() {
        $("input").removeAttr('disabled');
    }
</script>

<script>
    function updatePassword() {
        var data = {
            id: $('#id').val(),
            password: $('#newPassword').val()
        };

        $.ajax({
            type        : 'PUT', // define the type of HTTP verb we want to use (POST for our form)
            url         : '/updatePassword', // the url where we want to POST
            data        : data, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
            .done(function(data) {
                if(data.success){
                    alertify.success(data.message);
                    // window.location = "/";
                }else{
                    alert('fail');
                }
            })
            .fail(function(err){
                console.log(err);
            });
    }
</script>


<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->
    @yield('main-content')
</section><!-- /.content -->

<footer class="footer text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4 class="text-uppercase mb-4">Σχετικα με το Project</h4><hr>
                <p class="font-weight-light">Το παρόν site αποτελεί πτυχιακή εργασία του φοιτητή <strong>Αθανάσιου Σαραντίδη</strong> με ΑΕΜ 3368
                    για το ΤΕΙ Κεντρικής Μακεδονίας, Τμήμα Μηχανικών Πληροφορικής.

            </div>
            <div class="col-md-4 mb-5 mb-lg-0">
                {{--<h4 class="text-uppercase mb-4">Social mediaa</h4><hr>--}}
                {{--<ul class="footer-follow">--}}
                    {{--<li><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
                    {{--<li><a href="#"><i class="fa fa-twitter"></i></a></li>--}}
                    {{--<li><a href="#"><i class="fa fa-youtube"></i></a></li>--}}
                    {{--<li><a href="#"><i class="fa fa-instagram"></i></a></li>--}}
                {{--</ul>--}}


                <img class="mt-4" src="/img/teicm-logo.png" width="90">

                <p class="pt-4">Coded by <a href="#">Thanos Sara</a> </p>
            </div>

            <div class="col-md-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Βρειτε μας</h4><hr>
                <ul class="list-unstyled">
                    <li>Hairsalon</li>
                    <li>ΤΕΙ Κεντρικής Μακεδονίας</li>
                    <li>Μεραρχίας 103</li>
                    <li>Σέρρες</li>
                    <li>2310 20156</li>
                    <li>hairsalon@info.tk</li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="copyright text-center text-white">
    <div class="container">
        <small>Copyright &copy; HairSalon - TEICM 2017</small>
    </div>
</div>

@include('layouts.partials.scripts_front')
@yield('scripts')
</body>

</html>