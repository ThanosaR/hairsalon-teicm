<html>
    <head>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.min.css">
        <link rel="stylesheet" media="print" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.print.min.css">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
        <link href="{{ asset('css/homepage.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.min.css"/>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>

<body style="background-color: #f5f5f5;">


<nav id="navi" class="navbar navbar-default " style="border: #1a252f;">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                {{--<span class="sr-only">Toggle navigation</span>--}}
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/">Hair Salon</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/events">Ραντεβού <span class="sr-only">(current)</span></a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color: red; color: white">Γειά σου, <span class="hidden-xs">{{ Auth::user()->name }}!</span> <span class="caret"></span></a>

                    <ul class="dropdown-menu">
                        <a class="btn btn-primary" href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout" style="width: 100%;"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Αποσύνδεση
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        <input type="submit" value="logout" style="display: none;">
                        </form>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"> Δημιουργία Ραντεβού</h4>
                </div>
                <div id="newEvent" name="service" class="panel-body">
                    <form id="eventForm">
                        <div id="eventFormCopy">
                            <div class="form-group">
                                <label for="service">Υπηρεσία</label>
                                <select class="form-control" id="service" required="required">
                                    <option value="">Επιλέξτε μια υπηρεσία</option>
                                    @foreach($services as $service)
                                        <option value="{{$service["id"]}}" data-time="{{$service["time"]}}">{{$service["type"]}} ({{$service["time"]}}') - {{$service["cost"]}}€</option>
                                    @endforeach
                                </select>
                            </div>

                            @if(Auth::user()->hasRole('user'))
                                <div class="form-group">
                                    <label for="employee">Όνομα Υπαλλήλου</label>
                                    <select class="form-control" id="employee" required="required">
                                        <option value="">Επιλέξτε Υπάλληλο</option>
                                        @foreach($employees as $employee)
                                            <option value="{{$employee["id"]}}">{{$employee["name"]}} {{$employee["last_name"]}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif

                            @if(Auth::user()->hasRole('user'))
                                <div class="form-group">
                                    <label for="customer">Πελάτης</label>
                                    <input disabled class="form-control" type="text" name="" value="{{$user["name"]}} {{$user["last_name"]}}">
                                    <input class="form-control customer" type="hidden"  value="{{$user_id}}" required="required">
                                </div>
                            @elseif(Auth::user()->hasRole('employee'))
                                <div class="form-group">
                                    <label for="employee">Όνομα Πελάτη</label>
                                    <select class="form-control" id="employee" required="required">
                                        <option value="">Επιλέξτε Πελάτη</option>
                                        @foreach($users as $user)
                                            <option class="customer" value="{{$user_id}}">{{$user["name"]}} {{$user["last_name"]}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif




                            <div class="form-group">
                                <label for="start-at">Ημέρα, Ώρα έναρξης</label>
                                <input id="starts-at" class="form-control" type="text" name="daterange"  placeholder="Επιλέξτε μια ημερομηνία" required="required">
                            </div>
                            <div class="form-group">
                                <label for="end-at">Ημέρα, Ώρα λήξης</label>
                                <input id="ends-at" disabled class="form-control" type="text" name="daterange" placeholder="Επιλέξτε μια ημερομηνία λήξης" required="required">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Δημιουργία</button>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /. box -->

            @if(Auth::user()->hasRole('user'))
            <button class="btn btn-info userList" style="margin-bottom: 20px;"> Τα ραντεβού μου</button>
            @elseif(Auth::user()->hasRole('employee'))
            <button class="btn btn-info employeeList" style="margin-bottom: 20px;"> Τα ραντεβού μου</button>
            @endif

        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div><!-- /.row -->


    <div id="eventModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close dissmissEventModal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="modal-id" name="id" value="" />
                    <div class="form-group">
                        <label for="modal-service">Υπηρεσία</label>
                        <select disabled class="form-control" id="modal-service" required="required">
                            <option value="">Επιλέξτε μια υπηρεσία</option>
                            @foreach($services as $service)
                                <option value="{{$service["id"]}}" data-time="{{$service["time"]}}">{{$service["type"]}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="modalEmployee">Όνομα Υπαλλήλου</label>
                        <select disabled class="form-control" id="modal-employee" required="required">
                            <option value="">Επιλέξτε Υπάλληλο</option>
                            @foreach($employees as $employee)
                                <option value="{{$employee["id"]}}">{{$employee["name"]}} {{$employee["last_name"]}}</option>
                            @endforeach
                        </select>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="customer">Πελάτης</label>--}}
                        {{--<input disabled class="form-control" type="text" id="modal-customer" name="" value="{{$user["name"]}} {{$user["last_name"]}}" required="required">--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="modal-starts-at">Ημέρα, Ώρα έναρξης</label>
                        <input id="modal-starts-at" disabled class="form-control" type="text" name="daterange"  placeholder="Επιλέξτε μια ημερομηνία" required="required">
                    </div>
                    <div class="form-group">
                        <label for="modal-ends-at">Ημέρα, Ώρα λήξης</label>
                        <input id="modal-ends-at" disabled class="form-control" type="text" name="daterange" placeholder="Επιλέξτε μια ημερομηνία λήξης" required="required">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default dissmissEventModal">Κλείσιμο</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>

<div class="footer copyright text-center">
    <div class="container">
        <small class="text-white">Copyright &copy; HairSalon - TEICM 2017
            <p class="pt-4">Coded by <a href="#">Thanos Sara</a> </p></small>
    </div>
</div>

</body>


    <script src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

    {{--<script src="{!! asset("plugins/jquery-ui.min.js") !!}"></script>--}}
    <script src="{!! asset("js/partials/fullcalendar2-front.js") !!}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/fullcalendar.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.8.0/locale/el.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>--}}

    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>




</html>

