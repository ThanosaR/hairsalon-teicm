@include('layouts.partials.css_front')
<div id="loginform" class="card card-container">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Καρτέλες Σύνδεση - Εγγραφή -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#Login" role="tab">Σύνδεση</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#Registration" role="tab">Εγγραφή</a>
        </li>
    </ul>

    <!-- Τέλος - Καρτέλες Σύνδεση - Εγγραφή -->

    <img class="image-circle" src="img/hairsalon-logo.png">

    <br/>

    <!-- Login Form -->
    <div class="tab-content">
        <div class="tab-pane active" id="Login">
            <form role="form" class="form-horizontal" action="{{ url('/login') }}" method="POST" id="loginForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <login-input-field
                        name="{{ config('auth.providers.users.field','email') }}"
                        domain="{{ config('auth.defaults.domain','') }}"
                ></login-input-field>
                <div id="email-group" class="form-group">
                    <div class="cols-sm-10">
                        <div id="email-group" class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="email" class="form-control" name="email" id="email"  placeholder="Το Email σου" required>
                        </div>
                    </div>
                </div>

                <div id="password-group" class="form-group">
                    <div class="cols-sm-10">
                        <div id="password-group" class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password"  placeholder="Ο κωδικός σου" required>
                        </div>
                    </div>
                </div>

                <div id="remember" class="chebox">
                    <label>
                        <input type="checkbox" value="remember-me"> {{ trans('message.remember') }}
                    </label>
                </div>
                <div id="error-msg">    </div>
                <div>
                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Σύνδεση</button>
                </div>

                <a href="#" class="forgot-password">
                    {{ trans('message.forgotpassword') }}
                </a>
            </form>
        </div>
        <!-- End Login Form -->

                             <!-- Registration Form -->
        <div class="tab-pane" id="Registration">
            <form action="{{ url('/register') }}" method="POST" id="registrationForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Όνομα</label>
                    <div class="cols-sm-10">
                        <div id="name-group-r" class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" id="regname"  placeholder="Βάλε το Όνομα σου" name="name" required>
                        </div>
                    </div>

                </div>
                <div id="lastname-group-r" class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Επώνυμο</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user-plus" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" id="name"  placeholder="Βάλε το Επώνυμο σου" name="last_name" required>
                        </div>
                    </div>
                </div>

                <div id="email-group-r" class="form-group">
                    <label for="email" class="cols-sm-2 control-label">Email</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="email" class="form-control" id="email-reg"  placeholder="Βάλε το Email σου" name="email" value="{{ old('email') }}" required>
                        </div>
                    </div>
                </div>

                <div id="password-group-r" class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Κωδικός</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password-reg"  placeholder="Βάλε τον κωδικό σου" required>
                        </div>
                    </div>
                </div>

                <div id="repassword-group-r" class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Επιβεβαίωση κωδικού</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password_confirmation" id="password-repeat"  placeholder="Επιβεβαίωσε τον κωδικό σου" required>
                        </div>
                    </div>
                </div>
                <div class="form-check">
                    <label>
                        <div class="checkbox_register icheck">
                            <label>
                                <input type="checkbox" name="terms">Συμφωνώ με τους όρους
                            </label>
                        </div>
                    </label>
                </div>

                <div id="reg-error-msg">    </div>
                <div>
                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Εγγραφή</button>
                </div>
                <a href="#" class="forgot-password">
                    {{ trans('message.forgotpassword') }}
                </a>
            </form>
        </div>
    </div>
</div>

