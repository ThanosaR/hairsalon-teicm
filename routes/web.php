<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** Public routes */
Route::get('/', "HomeController@index");
Route::get('/checkUser', "HomeController@checkUser");
Route::put('/updateUser', "HomeController@updateUser");
Route::put('/updatePassword', "HomeController@updatePassword");



/** Public but authorized routes */
Route::group(['middleware' => 'auth'], function () {
    Route::view('/regcompleted', 'regcompleted');
    Route::group(['prefix' => 'events'], function (){
        Route::get('/', 'HomeController@indexEvent');
        Route::get('/getEvents', 'HomeController@getEvents');
        Route::get('/mineUser', 'HomeController@mineUser');
        Route::get('/{user_id}/user', 'HomeController@eventsListUser');
        Route::get('/mineEmployee', 'HomeController@mineEmployee');
        Route::get('/{employee_id}/user', 'HomeController@eventsListemployee');
        Route::post('add', 'HomeController@addEvent');
        Route::get('users', 'HomeController@getUsers');
        Route::get('employees', 'HomeController@getEmployees');
        Route::get('checkEvents', 'EventController@CheckForEvent');
    });

});

/** Admin routes */
Route::group(['middleware' => 'permission:admin_panel', "prefix" => 'admin'], function() {


    Route::get('/', function (\Illuminate\Http\Request $request) {
        return view('admin.home');
    });

    Route::group(['prefix' => 'events'], function (){
        Route::get('/', 'EventController@index');
        Route::get('/getEvents', 'EventController@getEvents');
        Route::get('/mine', 'EventController@mine');
        Route::get('/{employee_id}/employee', 'EventController@eventsList');
        Route::post('add', 'EventController@addEvent');
        Route::delete('del', 'EventController@delEvent');
        Route::put('update', 'EventController@updateEvent');
        Route::get('employees', 'EventController@getEmployees');

//    Route::post('events2', 'EventController@addEvent');
    });


    Route::get('/info', function (\Illuminate\Http\Request $request) {
        return view('admin.info');
    });

    Route::get('roots', 'RootsController@index' )->middleware('permission:root_panel');

    Route::group(['prefix' => 'services'], function() {
        Route::get('/', 'ServicesController@index');
        Route::get('/getServices', 'ServicesController@getServices')->name('services/getServices');
        Route::put('/createService', 'ServicesController@createService');
        Route::put('/updateService', 'ServicesController@updateService');
        Route::delete('/delete', 'ServicesController@deleteService');

    });

    Route::group(['prefix' => 'users'], function() {
        Route::get('/', 'UsersController@index');
        Route::get('/getUsers', 'UsersController@getUsers')->name('users/getUsers');
        Route::delete('/delete', 'UsersController@deleteUser');
        Route::put('/createUser', 'UsersController@createUser');
        Route::put('/updateUser', 'UsersController@updateUser');
    });

});